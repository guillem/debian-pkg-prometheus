# Copyright (C) 2018 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the prometheus package.
#
# Rui Branco - DebianPT <ruipb@debianpt.org>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: prometheus\n"
"Report-Msgid-Bugs-To: prometheus@packages.debian.org\n"
"POT-Creation-Date: 2020-07-07 22:16+0200\n"
"PO-Revision-Date: 2018-05-09 20:56+0000\n"
"Last-Translator: Rui Branco - DebianPT <ruipb@debianpt.org>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: boolean
#. Description
#: ../prometheus.templates:1001
msgid "Remove old Prometheus version 1 database files?"
msgstr "Remover a base de dados antiga da versão 1 do Prometheus?"

#. Type: boolean
#. Description
#: ../prometheus.templates:1001
msgid ""
"The data format in Prometheus 2.0 has completely changed and is not "
"backwards compatible with 1.x. Prometheus 2 will create a new data directory "
"in /var/lib/prometheus/metrics2. The old data in /var/lib/prometheus/metrics "
"can not be read by the new version."
msgstr ""
"O formato de dados no Prometheus 2.0 mudou completamente e não é compatível "
"com a versão anterior 1.x. O Prometheus 2 irá criar um novo directório de "
"dados em /var/lib/prometheus/metrics2. Os dados antigos em /var/lib/"
"prometheus/metrics não poderão ser lidos pela nova versão."

#. Type: boolean
#. Description
#: ../prometheus.templates:1001
msgid ""
"There is no conversion tool; if you want to retain access to the old data, "
"it is necessary to run a separate Prometheus 1.x instance on that data "
"directory. (This package makes no provision to allow this.)"
msgstr ""
"Não existe uma ferramenta de conversão; se quiser voltar a ter acesso aos "
"dados antigos será necessário correr uma instância separada do Prometheus 1."
"x naquele directório de dados. (Este pacote não fornece nada que permita "
"isto)."
